package ch.houseoftest;

import ch.houseoftest.game.Handsign;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class GameEngineTest {
    @Test
    /**
     * Verifying that all Handsigns can be returned - neither first nor last values are omitted.
     * Value distribution is not important.
     * With 3 signs and a sample of 100 generations the chances for failed test with correct code is ~2.46E-16%
     **/
    public void allHandsignsShouldBePossibleToDraw() {
        GameEngine ge = new GameEngine();
        Set<Handsign> resultSet = new HashSet<>();
        for (int i = 0; i < 100; i++) {
            resultSet.add(ge.getRandomHandsign());
        }
        assertThat(resultSet.size(), is(Handsign.values().length));
    }
}