package ch.houseoftest.game.feature;

import ch.houseoftest.Game;
import ch.houseoftest.GameEngine;
import ch.houseoftest.game.Handsign;
import ch.houseoftest.game.ui.ConsoleInterface;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class GameFeatureStepDefinition {
    Game game = new Game();
    GameEngine gameEngineMock = mock(GameEngine.class);
    ConsoleInterface uiMock = mock(ConsoleInterface.class);


    @Given("^Playing with computer$")
    public void playing_with_computer() {
        game.setGameEngine(gameEngineMock);
        game.setUi(uiMock);
    }

    @When("^I choose \"([^\"]*)\" handsign$")
    public void choose_handsign(Handsign handsign) {
        when(uiMock.askAndGetHandsignFromUser()).thenReturn(handsign);
    }

    @And("Computer picks \"([^\"]*)\" handsign$")
    public void computerPickROCK(Handsign handsign) {
        when(gameEngineMock.getRandomHandsign()).thenReturn(handsign);
    }

    @Then("^I win$")
    public void iWin() {
        game.playWithHuman();
        verify(uiMock).communicatePlayerWin(any(), any());
    }

    @Then("^I lose$")
    public void iLose() {
        game.playWithHuman();
        verify(uiMock).communicatePlayerLose(any(), any());
    }

    @Then("^I draw$")
    public void iDraw() {
        game.playWithHuman();
        verify(uiMock).communicateDraw(any());
    }
}