package ch.houseoftest.game;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class HandsignTest {
    @Test
    public void paperShouldBeatRock() {
        assertThat(Handsign.PAPER.isBeating(Handsign.ROCK), is(true));
    }

    @Test
    public void paperShouldNotBeatPaper() {
        assertThat(Handsign.PAPER.isBeating(Handsign.PAPER), is(false));
    }

    @Test
    public void paperShouldNotBeatScissors() {
        assertThat(Handsign.PAPER.isBeating(Handsign.SCISSORS), is(false));
    }

    @Test
    public void rockShouldNotBeatPaper() {
        assertThat(Handsign.ROCK.isBeating(Handsign.PAPER), is(false));
    }

    @Test
    public void rockShouldBeatScissors() {
        assertThat(Handsign.ROCK.isBeating(Handsign.SCISSORS), is(true));
    }

    @Test
    public void rockShouldNotBeatRock() {
        assertThat(Handsign.ROCK.isBeating(Handsign.ROCK), is(false));
    }

    @Test
    public void scissorsShouldNotBeatRock() {
        assertThat(Handsign.SCISSORS.isBeating(Handsign.ROCK), is(false));
    }

    @Test
    public void scissorsShouldNotBeatScissors() {
        assertThat(Handsign.SCISSORS.isBeating(Handsign.SCISSORS), is(false));
    }

    @Test
    public void scissorsShouldBeatPaper() {
        assertThat(Handsign.SCISSORS.isBeating(Handsign.PAPER), is(true));
    }

}