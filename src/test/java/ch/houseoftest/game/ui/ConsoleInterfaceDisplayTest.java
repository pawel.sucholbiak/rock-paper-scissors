package ch.houseoftest.game.ui;

import ch.houseoftest.game.GameType;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class ConsoleInterfaceDisplayTest {

    private ByteArrayOutputStream outputStream;

    @Before
    public void init() {
        outputStream = new ByteArrayOutputStream();
    }

    @Test
    public void introMessageShouldGreetPlayer() {
        ConsoleInterface ui = new ConsoleInterface(null, outputStream);
        ui.displayIntro();
        assertThat(outputStream.toString(StandardCharsets.UTF_8), containsString("Hello Player"));
    }

    @Test
    public void gameTypeQuestionShouldIncludeAllTypes() {
        ConsoleInterface ui = new ConsoleInterface(null, null);
        String question = ui.formulateGameTypeQuestion();

        // To fail test when enum was added/removed
        assertThat(GameType.values().length, is(3));

        assertThat(question, containsString(GameType.HUMAN_VS_COMPUTER.getLabel()));
        assertThat(question, containsString(GameType.COMPUTER_VS_COMPUTER.getLabel()));
        assertThat(question, containsString(GameType.EXIT.getLabel()));
    }

    @Test
    public void goodbyeMessageShouldThankPlayer(){
        ConsoleInterface ui = new ConsoleInterface(null, outputStream);

        ui.sayGoodbye();
        assertThat(outputStream.toString(StandardCharsets.UTF_8), containsString("Thanks"));
    }
}
