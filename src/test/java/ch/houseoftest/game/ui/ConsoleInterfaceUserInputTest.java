package ch.houseoftest.game.ui;

import ch.houseoftest.game.GameType;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class ConsoleInterfaceUserInputTest {

    private ByteArrayOutputStream outputStream;

    @Before
    public void init() {
        outputStream = new ByteArrayOutputStream();
    }

    @Test
    public void consoleUiShouldGetFirstGameTypeFromUser(){
        ByteArrayInputStream inputStream = new ByteArrayInputStream("1".getBytes(StandardCharsets.UTF_8));
        ConsoleInterface ui = new ConsoleInterface(inputStream, outputStream);
        GameType gameType = ui.askAndGetGameTypeFromUser();
        assertThat(gameType, is(GameType.HUMAN_VS_COMPUTER));
    }

    @Test
    public void consoleUiShouldGetSecondGameTypeFromUser(){
        ByteArrayInputStream inputStream = new ByteArrayInputStream("2".getBytes(StandardCharsets.UTF_8));
        ConsoleInterface ui = new ConsoleInterface(inputStream, outputStream);
        GameType gameType = ui.askAndGetGameTypeFromUser();
        assertThat(gameType, is(GameType.COMPUTER_VS_COMPUTER));
    }

    @Test(expected = UserInputException.class)
    public void consoleUiShouldThrowExceptionWhenUserWantsUnknownGameType(){
        ByteArrayInputStream inputStream = new ByteArrayInputStream("99".getBytes(StandardCharsets.UTF_8));
        ConsoleInterface ui = new ConsoleInterface(inputStream, outputStream);
        ui.askAndGetGameTypeFromUser();
    }

    @Test(expected = UserInputException.class)
    public void consoleUiShouldThrowExceptionWhenUserTypesString(){
        ByteArrayInputStream inputStream = new ByteArrayInputStream("no idea".getBytes(StandardCharsets.UTF_8));
        ConsoleInterface ui = new ConsoleInterface(inputStream, outputStream);
        ui.askAndGetGameTypeFromUser();
    }
}