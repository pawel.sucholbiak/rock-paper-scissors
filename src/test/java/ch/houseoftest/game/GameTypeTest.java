package ch.houseoftest.game;

import org.junit.Test;


public class GameTypeTest {

    @Test(expected = IllegalArgumentException.class)
    public void getByIdShouldThrowExceptionForUnknownId() {
        GameType.getById(99);
    }
}