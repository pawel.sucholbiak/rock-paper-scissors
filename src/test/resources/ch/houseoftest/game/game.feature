Feature: Play a game with a computer

  Scenario: Winning with computer using Paper against Rock
    Given Playing with computer
    When I choose "PAPER" handsign
    And Computer picks "ROCK" handsign
    Then I win

  Scenario: Loosing with computer using Paper against Scissors
    Given Playing with computer
    When I choose "PAPER" handsign
    And Computer picks "SCISSORS" handsign
    Then I lose

  Scenario: Drawing with computer when both are using Rock
    Given Playing with computer
    When I choose "ROCK" handsign
    And Computer picks "ROCK" handsign
    Then I draw
