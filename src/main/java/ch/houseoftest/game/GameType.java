package ch.houseoftest.game;

public enum GameType {
    HUMAN_VS_COMPUTER(1, "Human vs Computer"), COMPUTER_VS_COMPUTER(2, "Computer vs Computer"),
    // convenient to put EXIT as GameType
    EXIT(9, "Exit");

    private int id;
    private String label;

    GameType(int id, String label) {
        this.id = id;
        this.label = label;
    }

    public static GameType getById(int id) {
        for (GameType gameType : GameType.values()) {
            if (gameType.id == id) {
                return gameType;
            }
        }
        throw new IllegalArgumentException("Unknown GameType ID");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
