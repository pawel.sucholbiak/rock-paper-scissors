package ch.houseoftest.game;

public enum Handsign {
    ROCK(1), PAPER(2), SCISSORS(3);

    private int id;

    Handsign(int id) {
        this.id = id;
    }

    public static Handsign getById(int id) {
        for (Handsign handsign : Handsign.values()) {
            if (handsign.id == id) {
                return handsign;
            }
        }
        throw new IllegalArgumentException("Unknown Handsign ID");
    }

    public boolean isBeating(Handsign handsign) {
        switch (this) {
            case ROCK:
                return handsign == SCISSORS;
            case PAPER:
                return handsign == ROCK;
            case SCISSORS:
                return handsign == PAPER;
            default:
                return false;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
