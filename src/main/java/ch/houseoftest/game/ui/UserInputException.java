package ch.houseoftest.game.ui;

public class UserInputException extends RuntimeException {
    public UserInputException(String s) {
        super(s);
    }
}
