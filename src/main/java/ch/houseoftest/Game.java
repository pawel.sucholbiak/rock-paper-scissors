package ch.houseoftest;

import ch.houseoftest.game.GameType;
import ch.houseoftest.game.Handsign;
import ch.houseoftest.game.ui.ConsoleInterface;
import ch.houseoftest.game.ui.UserInputException;
import ch.houseoftest.game.ui.UserInterfaceIface;

public class Game {
    private UserInterfaceIface ui;
    private GameEngine gameEngine;

    public static void main(String[] args) {
        new Game().play();
    }

    public Game() {
        this.ui = new ConsoleInterface(System.in, System.out);
        this.gameEngine = new GameEngine();
    }

    private void play() {
        try {
            ui.displayIntro();
            GameType gameType;
            do {
                gameType = ui.askAndGetGameTypeFromUser();
                switch (gameType) {
                    case HUMAN_VS_COMPUTER:
                        playWithHuman();
                        break;
                    case COMPUTER_VS_COMPUTER:
                        watchComputersPlay();
                        break;
                    case EXIT:
                        break;
                    default:
                        ui.informAboutUnknownGameType();
                        // log which gametype has not been covered
                        break;
                }
            } while (GameType.EXIT != gameType);

        } catch (UserInputException ex) {
            // log what user has done wrong
        } finally {
            ui.sayGoodbye();
        }
    }

    public void watchComputersPlay() {
        Handsign firstHandsign = gameEngine.getRandomHandsign();
        Handsign secondHandsign = gameEngine.getRandomHandsign();

        if (firstHandsign.isBeating(secondHandsign) ^ secondHandsign.isBeating(firstHandsign)) {
            if (firstHandsign.isBeating(secondHandsign)) {
                ui.communicateComputersWin("Computer 1", firstHandsign, secondHandsign);
            } else {
                ui.communicateComputersWin("Computer 2", secondHandsign, firstHandsign);
            }
        } else {
            ui.communicateDraw(firstHandsign);
        }
    }

    public void playWithHuman() {
        Handsign playerHandsign = ui.askAndGetHandsignFromUser();
        Handsign computerHandsign = gameEngine.getRandomHandsign();

        if (playerHandsign.isBeating(computerHandsign) ^ computerHandsign.isBeating(playerHandsign)) {
            if (playerHandsign.isBeating(computerHandsign)) {
                ui.communicatePlayerWin(playerHandsign, computerHandsign);
            } else {
                ui.communicatePlayerLose(computerHandsign, playerHandsign);
            }
        } else {
            ui.communicateDraw(playerHandsign);
        }
    }

    public void setUi(UserInterfaceIface ui) {
        this.ui = ui;
    }

    public void setGameEngine(GameEngine gameEngine) {
        this.gameEngine = gameEngine;
    }
}
