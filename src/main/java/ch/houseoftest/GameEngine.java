package ch.houseoftest;

import ch.houseoftest.game.Handsign;

import java.util.Random;

public class GameEngine {

    private final Random random = new Random();

    public GameEngine() {
        random.setSeed(System.nanoTime());
    }

    public Handsign getRandomHandsign() {
        int maxValue = Handsign.values().length;
        int randomIndex = random.nextInt(maxValue);
        return Handsign.values()[randomIndex];
    }
}