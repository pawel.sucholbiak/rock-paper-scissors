```
__________               __            __________                                        _________      .__                                  
\______   \ ____   ____ |  | __        \______   \_____  ______   ___________           /   _____/ ____ |__| ______ _________________  ______
|       _//  _ \_/ ___\|  |/ /  ______ |     ___/\__  \ \____ \_/ __ \_  __ \  ______  \_____  \_/ ___\|  |/  ___//  ___/  _ \_  __ \/  ___/
|    |   (  <_> )  \___|    <  /_____/ |    |     / __ \|  |_> >  ___/|  | \/ /_____/  /        \  \___|  |\___ \ \___ (  <_> )  | \/\___ \
|____|_  /\____/ \___  >__|_ \         |____|    (____  /   __/ \___  >__|            /_______  /\___  >__/____  >____  >____/|__|  /____  >
       \/            \/     \/                        \/|__|        \/                        \/     \/        \/     \/                 \/
```
## Rock-Paper-Scissors Game
### Game Rules
[Wikipedia - Rock paper scissors](https://en.wikipedia.org/wiki/Rock_paper_scissors)

### Requirements
* JDK 11
* Maven 3.8.+

### Build the package
```
 mvn clean package
```

### Run tests
```
mvn test
```

### Running the game
1. Build the package
2. Run the game with ```fat-jar```:
```
java -jar target/rock-paper-scissors-jar-with-dependencies.jar
```
or run it with classpath:
```
java -cp target/rock-paper-scissors.jar ch.houseoftest.Game
```

### Author
Pawel Sucholbiak <pawel.sucholbiak@gmail.com>
\
[LinkedIn Pawel Sucholbiak](https://www.linkedin.com/in/pawelsucholbiak)